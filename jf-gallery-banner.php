<?php
/**
 * Plugin Name: Gallery Banner
 * Plugin URI: https://www.jfwebdesign.com/
 * Description: This plugin allows you to create a full frame banner using multiple images.
 * Version: 1.0
 * Author: JF WebDesign
 * Author URI: https://www.jfwebdesign.com/
 */
// Register and load the widget
function jf_load_gallery_banner_widget() {
    register_widget( 'sp_widget_gallery_banner' );
}
add_action( 'widgets_init', 'jf_load_gallery_banner_widget' );
 
// Creating the widget 
class sp_widget_gallery_banner extends WP_Widget {
 
function __construct() {
parent::__construct(
 
// Base ID of your widget
'sp_widget_gallery_banner', 
 
// Widget name will appear in UI
__('Gallery Banner', 'sp_widget_gallery_banner_domain'), 
 
// Widget description
array( 'description' => __( 'Add a gallery banner to widget.', 'sp_widget_gallery_banner_domain' ), ) 
);
}
 
// Creating widget front-end
 
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
$image = $instance['image'];
$size = $instance['size'];
$bheight = $instance['bheight'];
$mbheight = $instance['mbheight'];
$iwidth = $instance['iwidth'];
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];
	// This is where you run the code and display the output
 ?>
<?php if($image):
	$images = explode(',', $image);
	?>

<style>
	.<?php echo $this->get_field_id( 'image' ); ?> {display: flex; width: 100vw; height: <?php echo $bheight; ?>; overflow: scroll;}
	.<?php echo $this->get_field_id( 'image' ); ?>::-webkit-scrollbar { 
                display: none; 
            } 
	.<?php echo $this->get_field_id( 'image' ); ?> img  {height: <?php echo $bheight; ?>;}

	@media screen and (max-width: 728px) {
		.<?php echo $this->get_field_id( 'image' ); ?> {width: 100%; height: <?php echo $mbheight; ?>; overflow: scroll;}
		.<?php echo $this->get_field_id( 'image' ); ?>::-webkit-scrollbar { 
                display: none; 
            } 
		.<?php echo $this->get_field_id( 'image' ); ?> img{height: <?php echo $mbheight; ?>; }
		
	}
</style>
	<?php
	echo '<div class="'.$this->get_field_id( 'image' ).'">';
	foreach ($images as $id) {
		
    	$image_attributes = wp_get_attachment_image_src( $id, $size );
			if( $image_attributes ) {
				if ($image_attributes[2] >$image_attributes[1]) {$ratio = $iwidth  * ($image_attributes[2] / $image_attributes[1]);}
				if ($image_attributes[1] >$image_attributes[2]) {$ratio = $iwidth  * ($image_attributes[1] / $image_attributes[2]);}
				echo '<img src="'. $image_attributes[0] .'"style="width:'.$ratio.' ;">';
			}
	}
	echo '</div>';
?>
   <?php endif; ?>
 
   <?php	
echo $args['after_widget'];
}
         
// Widget Backend 
public function form( $instance ) {
$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( '', 'text_domain' ) : $instance['title'] );
$image = ! empty( $instance['image'] ) ? $instance['image'] : '';
$size = ! empty( $instance['size'] ) ? $instance['size'] : '';
$bheight = ! empty( $instance['bheight'] ) ? $instance['bheight'] : '';
$mbheight = ! empty( $instance['mbheight'] ) ? $instance['mbheight'] : '';
$iwidth = ! empty( $instance['iwidth'] ) ? $instance['iwidth'] : '';
   ?>
   <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
   </p>
   <p>
      <label for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e( 'Image:' ); ?></label><br>
      <input class="widefat" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" type="text" value="<?php echo $image ; ?>" />
	   <br>
      <button class="upload_image_button button button-primary">Select Images</button>
   </p>
<p>
  <label for="<?php echo $this->get_field_id( 'bheight' ); ?>"><?php _e( 'Banner Height:' ); ?></label>
  <input class="widefat" id="<?php echo $this->get_field_id( 'bheight' ); ?>" name="<?php echo $this->get_field_name( 'bheight' ); ?>" type="text" value="<?php echo esc_attr( $bheight); ?>">
</p>
<p>
  <label for="<?php echo $this->get_field_id( 'mbheight' ); ?>"><?php _e( 'Banner Height (mobile):' ); ?></label>
  <input class="widefat" id="<?php echo $this->get_field_id( 'mbheight' ); ?>" name="<?php echo $this->get_field_name( 'mbheight' ); ?>" type="text" value="<?php echo esc_attr( $mbheight); ?>">
</p>
<p>
  <label for="<?php echo $this->get_field_id( 'iwidth' ); ?>"><?php _e( 'Image Width:' ); ?></label>
  <input class="widefat" id="<?php echo $this->get_field_id( 'iwidth' ); ?>" name="<?php echo $this->get_field_name( 'iwidth' ); ?>" type="text" value="<?php echo esc_attr( $iwidth); ?>">
</p>
 <p>
      <label for="<?php echo $this->get_field_id( 'size' ); ?>"><?php _e( 'Image Size:' ); ?></label>
		<select id="<?php echo $this->get_field_id( 'size' ); ?>" name="<?php echo $this->get_field_name( 'size' ); ?>">
			<option value="full" <?php if ($size == 'full'){echo 'selected';} ?>>Full</option>
			<option value="large" <?php if ($size == 'large'){echo 'selected';} ?>>Large</option>
			<option value="medium" <?php if ($size == 'medium'){echo 'selected';} ?>>Medium</option>
		</select>
   </p>
   <?php
}
     
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['image'] = ( ! empty( $new_instance['image'] ) ) ? $new_instance['image'] : '';
		$instance['size'] = ( ! empty( $new_instance['size'] ) ) ? $new_instance['size'] : '';
		$instance['bheight'] = ( ! empty( $new_instance['bheight'] ) ) ? $new_instance['bheight'] : '';
		$instance['mbheight'] = ( ! empty( $new_instance['mbheight'] ) ) ? $new_instance['mbheight'] : '';
		$instance['iwidth'] = ( ! empty( $new_instance['iwidth'] ) ) ? $new_instance['iwidth'] : '';
   return $instance;
}


	
} // Class sp_widget_gallery_banner ends here

function jf_gallery_banner_script() {   
	wp_enqueue_script( 'media-upload' );
	wp_enqueue_media();
	wp_enqueue_script( 'jf_gallery_banner_script', plugin_dir_url( __FILE__ ) . 'jf_gallery_banner_script.js', array('jquery'), '1.0' );
}
add_action('admin_enqueue_scripts', 'jf_gallery_banner_script');
?>
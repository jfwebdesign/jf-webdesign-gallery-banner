jQuery(document).ready(function ($) {
 
   $(document).on("click", ".upload_image_button", function (e) 
		{
				e.preventDefault();
				var $button = $(this);
				// Create the media frame.
var file_frame = wp.media.frames.file_frame = wp.media ({title: 'Select or upload image', library: {type: 'image'}, button: {text: 'Select' }, multiple: true});   
				// When an image is selected, run a callback.
				file_frame.on('select', function () {
								// We set multiple to false so only get one image from the uploader
								var attachments = file_frame.state().get('selection').map( 
									function( attachments ) {
										attachments.toJSON();
										return attachments;
								});
									var i;
									var imageID = '';
									for (i = 0; i < attachments.length; ++i) 
										{
											if (imageID !='') {
											imageID = imageID + ','+ attachments[i].attributes.id;
											}
											else {
												imageID = attachments[i].attributes.id;
											}
										}
								$button.siblings('input').val(imageID);
								$button.siblings('input').trigger("change");
							});
file_frame.on('open', function(){
		var selection = file_frame.state().get('selection');
		var selected = $button.siblings('input').val(); // the id of the image
		if (selected) {
			var nameArr = selected.split(',');
			var c;
			for(c = 0; c < nameArr.length; ++c) { 
			var	thisImage = nameArr[c];
  				selection.add(wp.media.attachment(thisImage));
			}
		}
});
// Finally, open the modal
file_frame.open();
   });
});